package net.komputerking.pms;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import net.komputerking.pms.api.PMSPlugin;
import net.komputerking.pms.exceptions.PluginDependencyNotFoundException;
import net.komputerking.pms.objects.PluginDescriptionFile;
import net.komputerking.pms.objects.Settings;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Class to manage loading plugins.
 */
public class PluginManager {
    
    public List<PMSPlugin> plugins = new ArrayList<PMSPlugin>();
    
    public void loadPlugins() throws Exception {
        Serializer serial = new Persister();
        for (File f : new File(Settings.getSettings().getPluginPath()).listFiles()){
            if (f.getName().endsWith(".jar")){
                URLClassLoader cl = URLClassLoader.newInstance(new URL[]{f.toURI().toURL()});
                PluginDescriptionFile data = serial.read(PluginDescriptionFile.class, cl.getResource("plugin.xml").openStream());
                Class<?> clazz = cl.loadClass(data.getMain());
                PMSPlugin plugin = (PMSPlugin) clazz.newInstance();
                plugin.descriptionFile = data;
                plugins.add(plugin);
            }
        }
    }
    
    public void enablePlugin(PMSPlugin pl) {
        pl.onEnable();
        pl.isEnabled = true;
    }
    
    public void enablePlugins() throws PluginDependencyNotFoundException {
        for (PMSPlugin p : plugins){
           if (p.getDependencies() != null){
               for (String s : p.getDependencies()){
                   PMSPlugin dependency = null;
                   for (PMSPlugin pl : plugins){
                       if (p.descriptionFile.getName().toLowerCase().equals(s.toLowerCase())){
                           dependency = pl;
                           break;
                       }
                   }
                   if (dependency == null){
                       throw new PluginDependencyNotFoundException(s, p.descriptionFile.getName());
                   } else {
                       if (!dependency.isEnabled){
                           enablePlugin(dependency);
                           enablePlugin(p);
                       } else {
                           enablePlugin(p);
                       }
                   }
               }
           } else {
               enablePlugin(p);
           }
        }
    }
    
}
