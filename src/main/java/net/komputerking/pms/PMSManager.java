package net.komputerking.pms;

import java.io.File;
import net.komputerking.pms.objects.PupilMap;
import net.komputerking.pms.objects.Settings;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class PMSManager {

    public PMSManager() throws Exception {
        System.out.println("*** Launching PMS ***");
        System.out.print("0/3 - Loading Settings");
        Serializer serial = new Persister();
        File data = new File("settings.dat");
        if (data.exists()) {
            Settings.setInstance(serial.read(Settings.class, data));
        } else {
            Settings temp = new Settings();
            temp.setDefaults();
            serial.write(temp, data);
            Settings.setInstance(temp);
        }
        data = new File(Settings.getSettings().getDataPath());
        if (!data.isDirectory()) {
            data.mkdirs();
        }
        System.out.println("1/3 - Loading Pupil Information");
        data = new File(Settings.getSettings().getDataPath() + "/pupils.dat");
        if (!data.exists()) {
            data.createNewFile();
            PupilMap temp = new PupilMap();
            temp.setDefaults();
            serial.write(temp, data);
            PupilMap.setInstance(temp);
        } else {
            PupilMap.setInstance(serial.read(PupilMap.class, data));
        }
        System.out.println("2/3 - Initializing Plugins");
        data = new File(Settings.getSettings().getPluginPath());
        if (!data.isDirectory()){
            data.mkdirs();
        }
        PMS.initializeAPI();
        PMS.getPluginManager().loadPlugins();
        PMS.getPluginManager().enablePlugins();
        System.out.println("3/3 - Initialized!\n");
        System.out.println("PMS is now running.");
    }

}
