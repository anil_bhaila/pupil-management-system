package net.komputerking.pms;

/**
 * PMS (Pupil Management System) launcher class.
 */
public class PMSLauncher {
    
    private static PMSManager manager;

    public static void main(String[] args) throws Exception {
        manager = new PMSManager();
    }
    
}
