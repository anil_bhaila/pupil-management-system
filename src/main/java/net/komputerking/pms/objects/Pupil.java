package net.komputerking.pms.objects;

import lombok.Data;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Pupil object class.
 */
@Data
@Root(name = "pupil", strict = false)
public class Pupil {
    
    @Attribute
    private String form;
    
    @Element
    private String firstName;
    
    @Element
    private String lastName;
    
}
