package net.komputerking.pms.objects;

import java.io.File;
import lombok.Data;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * XML Object to store settings.
 */
@Data
@Root(name = "settings", strict = false)
public class Settings {
    
    private static Settings instance;
    
    @Element
    private String dataPath;
    
    @Element
    private String pluginPath;
    
    public void setDefaults(){
        dataPath = new File("data").getPath();
        pluginPath = new File("plugins").getPath();
    }
    
    public static void setInstance(Settings par1){
        instance = par1;
    }
    
    public static Settings getSettings(){
        return instance;
    }
    
}
