package net.komputerking.pms.objects;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * XML file for pupils.dat
 */
@Data
@Root(name = "pmsdata", strict = false)
public class PupilMap {
    
    private static PupilMap instance;
    
    @ElementList
    private List<Pupil> pupils;
    
    public void setDefaults(){
        pupils = new ArrayList<Pupil>();
    }
    
    public static void setInstance(PupilMap par1){
        instance = par1;
    }
    
    public static PupilMap getPupilMap(){
        return instance;
    }
    
}
