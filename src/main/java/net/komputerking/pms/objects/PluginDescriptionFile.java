package net.komputerking.pms.objects;

import lombok.Data;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * plugin.xml Object.
 */
@Data
@Root(name = "plugin", strict = false)
public class PluginDescriptionFile {
    
    @Element
    private String name;
    
    @Element
    private String main;
    
    @Element
    private String authors;
    
    private boolean enabled;
    
}
