package net.komputerking.pms.api.events;

/**
 * Interface to make an event cancellable.
 */
public interface Cancellable {
    
    public void setCancelled(boolean b);
    public boolean getCancelled();
   
}
