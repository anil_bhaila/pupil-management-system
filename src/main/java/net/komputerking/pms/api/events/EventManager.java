package net.komputerking.pms.api.events;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to handle events.
 */
public class EventManager {
    
    private List<Listener> registry = new ArrayList<Listener>();
    
    public void registerEvents(Listener l) {
        registry.add(l);
    }
    
    public void callEvent(Event e) {
        try {
        for (Listener l : registry){
            Class<? extends Listener> clazz = l.getClass();
            for (Method m : clazz.getDeclaredMethods()){
                if (m.isAnnotationPresent(EventHandler.class)){
                    EventHandler annotation = (EventHandler) m.getAnnotation(EventHandler.class);
                    if (e.getClass() == annotation.event()){
                        m.invoke(l, e);
                    }
                }
            }
        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
