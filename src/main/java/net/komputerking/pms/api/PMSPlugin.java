package net.komputerking.pms.api;

import net.komputerking.pms.objects.PluginDescriptionFile;

/**
 * Main Plugin class.
 */
public abstract class PMSPlugin {
    
    public PluginDescriptionFile descriptionFile;
    public boolean isEnabled = false;
    
    public void onEnable(){}
    public void onDisable(){}
    public String[] getDependencies(){return null;};
    
}
