package net.komputerking.pms.exceptions;

/**
 * Exception thrown when a dependency for a plugin is not found.
 */
public class PluginDependencyNotFoundException extends Exception {
    
    public PluginDependencyNotFoundException(String par1, String par2) {
        super("Could not find the dependency '" + par1 + "' for '" + par2 + "'.");
    }

}
