package net.komputerking.pms;

import net.komputerking.pms.api.events.EventManager;

/**
 * PMS class for API plugins to use.
 */
public class PMS {
    
    private static EventManager eventmanager;
    private static PluginManager pluginmanager;
    
    public static void initializeAPI() {
        eventmanager = new EventManager();
        pluginmanager = new PluginManager();
    }
    
    public static EventManager getEventManager() {
        return eventmanager;
    }
    
    public static PluginManager getPluginManager() {
        return pluginmanager;
    }
    
}
